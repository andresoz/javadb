package com.platzi.mensajesApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDb {
	
	public Connection get_connection() {
		Connection connection = null;
		
		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mensajes_app",
					"root",
					""
					);
			if (connection != null) {
				System.out.println("Conexi�n exitosa");
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		
		return connection;
	}
}
